from django.urls import path
from rest_framework.authtoken import views as tokenview

from . import views

urlpatterns = [
    path("emp/", views.EmployeeViewApi.as_view()),
    path("emp/<int:pk>", views.EmployeeViewDetail.as_view()),
    path("get-token/", tokenview.obtain_auth_token),
]
