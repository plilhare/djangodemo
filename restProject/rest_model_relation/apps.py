from django.apps import AppConfig


class ApiConfig(AppConfig):
    name = "rest_model_relation"
