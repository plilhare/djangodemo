from django.urls import path
from . import views


urlpatterns = [
    # path('create/', views.employee_create),
    path("emp/", views.EmployeeViewApi.as_view()),
    path("emp/<int:pk>", views.EmployeeViewDetail.as_view()),
    path("profile/", views.ProfileViewApi.as_view()),
    path("profile/<int:pk>", views.ProfileViewDetail.as_view()),
    path("projects/", views.ProjectViewApi.as_view()),
    path("projects/<int:pk>", views.ProjectViewDetail.as_view()),
    path("company/", views.CompanyViewApi.as_view()),
    path("company/<int:pk>", views.CompanyViewDetail.as_view()),
]
