from django.contrib import admin
from .models import Employee, Profile, Projects, Company

# Register your models here.


@admin.register(Employee)
class EmployeeAdmin(admin.ModelAdmin):
    list_display = ["id", "name", "age", "city"]


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ["employee", "address", "phone_number"]


@admin.register(Projects)
class ProjectAdmin(admin.ModelAdmin):
    list_display = ["employee", "name", "domain"]


@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    list_display = ["name", "address"]
