# Generated by Django 3.1.7 on 2021-03-16 12:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("rest_model_relation", "0005_auto_20210316_1203"),
    ]

    operations = [
        migrations.CreateModel(
            name="Company",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("name", models.CharField(max_length=100)),
                ("address", models.CharField(max_length=180)),
                ("employee", models.ManyToManyField(to="rest_model_relation.Employee")),
            ],
        ),
    ]
