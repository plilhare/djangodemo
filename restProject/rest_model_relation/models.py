from django.db import models


# Create your models here.
class Employee(models.Model):
    name = models.CharField(max_length=100)
    age = models.IntegerField()
    city = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Profile(models.Model):
    employee = models.OneToOneField(Employee, on_delete=models.CASCADE)
    address = models.CharField(max_length=180)
    phone_number = models.CharField(max_length=12)


class Projects(models.Model):
    employee = models.ForeignKey(Employee,
                                 null=False,
                                 on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    domain = models.CharField(max_length=50)


class Company(models.Model):
    employee = models.ManyToManyField(Employee)
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=180)
