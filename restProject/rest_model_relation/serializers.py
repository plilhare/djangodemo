from rest_framework import serializers
from .models import Employee, Profile, Projects, Company


def alpha_name(value):
    if not value.isalpha():
        raise serializers.ValidationError(
            "Name should not contain numbers or special characters"
        )
    return value


def positive_age(value):
    if value < 0:
        raise serializers.ValidationError("Age should not be negative")
    return value


class EmployeeSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=100, validators=[alpha_name])
    age = serializers.IntegerField(validators=[positive_age])
    city = serializers.CharField(max_length=100)

    def create(self, validated_data):
        return Employee.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.name = validated_data.get("name", instance.name)
        instance.age = validated_data.get("age", instance.age)
        instance.city = validated_data.get("city", instance.city)
        instance.save()
        return instance


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ["address", "phone_number", "employee"]


class ProjectSerializer(serializers.Serializer):
    employee = serializers.PrimaryKeyRelatedField(
        many=False, queryset=Employee.objects.all()
    )
    name = serializers.CharField(max_length=100)
    domain = serializers.CharField(max_length=50)

    def create(self, validated_data):
        return Projects.objects.create(**validated_data)


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ["name", "address", "employee"]
