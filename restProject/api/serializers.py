from rest_framework import serializers
from .models import Employee


def alpha_name(value):
    if not value.isalpha():
        raise serializers.ValidationError(
            "Name should not contain numbers or special characters"
        )
    return value


def positive_age(value):
    if value < 0:
        raise serializers.ValidationError("Age should not be negative")
    return value


class EmployeeSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=100, validators=[alpha_name])
    age = serializers.IntegerField(validators=[positive_age])
    city = serializers.CharField(max_length=100)

    def create(self, validated_data):
        return Employee.objects.create(**validated_data)
