from django.contrib import admin
from .models import Employee

# Register your models here.


@admin.register(Employee)
class EmployeeAdmmin(admin.ModelAdmin):
    list_display = ["id", "name", "age", "city"]
