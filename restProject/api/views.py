from django.http.response import JsonResponse
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser

from .models import Employee
from .serializers import EmployeeSerializer


# Create your views here.
@api_view(["GET", "POST", "DELETE", "PUT"])
def view_api(request, pk=None):
    if request.method == "GET":
        emp = Employee.objects.all()
        employee_serializer = EmployeeSerializer(emp, many=True)
        return JsonResponse(employee_serializer.data, safe=False)

    elif request.method == "POST":
        employee_data = JSONParser().parse(request)
        employee_serializer = EmployeeSerializer(data=employee_data)
        if employee_serializer.is_valid():
            employee_serializer.save()
            return JsonResponse(
                employee_serializer.data, status=status.HTTP_201_CREATED
            )
        return JsonResponse(
            employee_serializer.errors, status=status.HTTP_400_BAD_REQUEST
        )

    elif request.method == "PUT":
        employee_data = JSONParser().parse(request)
        employee_serializer = EmployeeSerializer(data=employee_data)
        if employee_serializer.is_valid():
            employee_serializer.save()
            return JsonResponse(employee_serializer.data)
        return JsonResponse(
            employee_serializer.errors, status=status.HTTP_400_BAD_REQUEST
        )

    elif request.method == "DELETE":
        emp = Employee.objects.get(pk=pk)
        emp.delete()
        return JsonResponse(
            {"message": "Tutorial was deleted successfully!"},
            status=status.HTTP_204_NO_CONTENT,
        )
