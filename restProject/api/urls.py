from django.urls import path
from django.conf.urls import url
from . import views

urlpatterns = [
    # path('create/', views.employee_create),
    path("emp/", views.view_api),
    url(r"^emp/(?P<pk>[0-9]+)$", views.view_api),
]
