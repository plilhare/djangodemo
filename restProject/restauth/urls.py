from django.urls import path
from . import views


urlpatterns = [
    path("emp/", views.EmployeeViewApi.as_view()),
    path("emp/<int:pk>", views.EmployeeViewDetail.as_view()),
]
