from django.shortcuts import render, redirect
from employee.forms import EmployeeForm
from employee.models import Employees
from django.core.paginator import Paginator
# Create your views here.


def emp(request):
    if request.method == "POST":
        form = EmployeeForm(request.POST)

        if form.is_valid():
            try:
                form.save()
                return redirect("/show")
            except Exception as e:
                print(e)
    else:
        form = EmployeeForm()
    return render(request, "index.html", {"form": form})


def show(request):
    employees = Employees.objects.all().order_by('eid')
    paginator = Paginator(employees, 3)
    page_number = request.GET.get("page")
    page_obj = paginator.get_page(page_number)
    return render(request, "show.html", {"page_obj": page_obj})


def edit_data(request, id):
    employee = Employees.objects.get(eid=id)
    form = EmployeeForm(request.POST, instance=employee)
    if form.is_valid():
        form.save()
        return redirect("/show")
    return render(request, "edit.html", {"employee": employee})


def destroy(request, id):
    employee = Employees.objects.get(eid=id)
    employee.delete()
    return redirect("/show")
