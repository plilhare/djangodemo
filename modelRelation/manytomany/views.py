from django.shortcuts import render
from .models import User, Article
# Create your views here.


def home(request):
    user = User.objects.all().order_by("id")
    page = Article.objects.all()
    context = {'users': user,
               'pages': page,
               'relation': 'Many To Many Relationship'}

    return render(request, "showMtoM.html", context)
