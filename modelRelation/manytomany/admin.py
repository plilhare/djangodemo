from django.contrib import admin
from .models import Article
# Register your models here.


@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    list_display = ['page_name',
                    'page_cat',
                    'page_publish_date',
                    'published_by']
