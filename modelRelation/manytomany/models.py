from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class Article(models.Model):
    user = models.ManyToManyField(User)
    page_name = models.CharField(max_length=70)
    page_cat = models.CharField(max_length=70)
    page_publish_date = models.DateField()

    def published_by(self):
        return ','.join([str(p) for p in self.user.all()])
