from django.shortcuts import render
from .models import User, Page
# Create your views here.


def home(request):
    user = User.objects.all()
    page = Page.objects.all()
    context = {
        'users': user, 'pages': page, 'relation': 'One To One Relationship'
        }
    return render(request, "show.html", context)
