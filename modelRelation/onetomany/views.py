from django.shortcuts import render
from .models import User, Post
# Create your views here.


def home(request):
    user = User.objects.all().order_by("id")
    page = Post.objects.all()
    context = {'users': user,
               'pages': page,
               'relation': 'One To Many Relationship'}

    return render(request, "show.html", context)
