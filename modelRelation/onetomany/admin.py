from django.contrib import admin
from .models import Post
# Register your models here.


@admin.register(Post)
class PageAdmin(admin.ModelAdmin):
    list_display = ['page_name', 'page_cat', 'page_publish_date', 'user']
